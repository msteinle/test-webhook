# Exercice 1

À partir de la donnée du rayon d'un cercle, faites calculer et afficher la surface et la circonférence de ce cercle.<br>


## Sorties produites:

```
Calcul de la surface et de la circonférence d'un cercle étant donné son rayon
Donnée:
  Rayon du cercle: rayon = 1.0 cm

  Surface du cercle: surface = 3.141592653589793 cm2
  Circonférence du cercle: circonférence = 6.283185307179586 cm
```
```
Calcul de la surface et de la circonférence d'un cercle étant donné son rayon
Donnée:
  Rayon du cercle: rayon = 10.0 cm

  Surface du cercle: surface = 314.1592653589793 cm2
  Circonférence du cercle: circonférence = 62.83185307179586 cm
```

Test lien vers fichier python:
[Solution](temp.py)
[plain text](temp.txt)