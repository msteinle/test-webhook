# === Précédence des opérateurs / priorité des opérations ===
1+2*3
# 1 + 6
# 7

# Expression qui a agité les réseaux sociaux en l'été 2019)
8/2*(2+2)
# 4.0 * (2+2)
# 4.0 * (4)
# 16.0
# ".0" et parenthèses autour du 4 optionnels

9//2*4%2
# 4*4%2
# 16%2
# 0

9//2+4%3
# 4+4%3
# 4+1
# 5

9//(2+4)%3
# 9//6%3
# 1%3
# 1

# === 


